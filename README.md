# langstrings: A small Java library for localising your applications

You put your language strings in a predefined Yaml template, and get a loader and methods for
retrieving the strings in a session-specific language.

A simple templating mechanism is provided.

Code is unit tested.

## Limitations

* The code is in `beta` stage. Use for production at your own risk.

## How to use

Documentation is to come.

Meanwhile, see how it is used in the examples:

* https://gitlab.com/gllona/talks-example

## What you need

* Java 1.8

### Dependencies

* `org.yaml:snakeyaml:1.26`
