package org.logicas.librerias.translations;

import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LangStringsFile {

    @Getter
    @Setter
    public static class Varieties {
        private String slug;
        private Map<String, String> variety;
    }

    private Map<String, String> langs;
    private List<Varieties> strings;
}
