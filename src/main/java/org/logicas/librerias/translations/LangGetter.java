package org.logicas.librerias.translations;

@FunctionalInterface
public interface LangGetter {

    String getLangForSession(Object session);
}
