package org.logicas.librerias.translations;

import java.util.HashMap;
import java.util.Map;

class Replacer {

    static class PartsKeeper {
        Map<String, String> parts = new HashMap<>();

        PartsKeeper with(String name, String value) {
            parts.put(name, value);
            return this;
        }

        String replace(String original) {
            String transformed = "#" + original + "#";
            boolean changed;
            do {
                changed = false;
                for (Map.Entry<String, String> entry : parts.entrySet()) {
                    String regexStr = "(?<!\\{)\\{" + entry.getKey() + "\\}(?!\\})";
                    String newTransformed = transformed.replaceAll(regexStr, entry.getValue());
                    if (! newTransformed.equals(transformed)) {
                        changed = true;
                    }
                    transformed = newTransformed;
                }
            } while (changed);
            return transformed.substring(1, transformed.length() - 1)
                .replaceAll("\\{\\{", "{")
                .replaceAll("\\}\\}", "}");
        }
    }

    private PartsKeeper partsKeeper;

    String replace(String string) {
        return partsKeeper().replace(string);
    }

    PartsKeeper with(String name, String value) {
        return partsKeeper().with(name, value);
    }

    synchronized PartsKeeper partsKeeper() {
        if (partsKeeper == null) {
            partsKeeper = new PartsKeeper();
        }
        return partsKeeper;
    }
}
