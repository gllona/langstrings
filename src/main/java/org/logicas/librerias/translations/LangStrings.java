package org.logicas.librerias.translations;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

public class LangStrings {

    public class Builder {

        private Map<String, String> parts = new HashMap<>();

        public Builder with(String name, String value) {
            parts.put(name, value);
            return this;
        }

        public String str(String slug, Object session) {
            String lang = langGetter.getLangForSession(session);
            if (lang == null || ! langStringsFile.getLangs().containsKey(lang)) {
                return null;
            }

            Optional<String> translation = langStringsFile.getStrings()
                .stream()
                .filter(varieties -> varieties.getSlug().equals(slug))
                .map(varieties -> varieties.getVariety().get(lang))
                .findFirst();

            Replacer replacer = buildReplacer(parts);

            return translation.map(replacer::replace)
                .orElse(null);
        }

        private Replacer buildReplacer(Map<String, String> parts) {
            Replacer replacer = new Replacer();
            for (Map.Entry<String, String> entry : parts.entrySet()) {
                replacer.partsKeeper().with(entry.getKey(), entry.getValue());
            }
            return replacer;
        }
    }

    private String yamlFileName;
    private LangGetter langGetter;
    private LangStringsFile langStringsFile;

    public LangStrings(String yamlFileName, LangGetter langGetter) {
        this.yamlFileName = yamlFileName;
        this.langGetter = langGetter;
        openYaml();
    }

    private void openYaml() {
        Yaml yaml = new Yaml(new Constructor(LangStringsFile.class));
        InputStream inputStream = this.getClass()
            .getClassLoader()
            .getResourceAsStream(yamlFileName);
        langStringsFile = yaml.load(inputStream);
    }

    public String str(String slug, Object session) {
        return new Builder().str(slug, session);
    }

    public Builder with(String name, String value) {
        return new Builder().with(name, value);
    }

    public Map<String, String> getLangs() {
        return langStringsFile.getLangs();
    }
}
