package org.logicas.librerias.translations;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LangStringsTest {

    static String BASE_YAML = "life_strings.yml";

    static String STR01 = "life-started-before";
    static String STR01_EN = "On the contrary, non human life arise before";
    static String STR01_ES = "Por el contrario, vida no humana empezó antes";
    static String STR02 = "plants-alone";
    static String STR02_EN = "Plants were alone for eons";
    static String STR02_ES = "Las plantas fueron solas por eones";
    static String STR_NOT_FOUND = "life_ends";
    static String STR03 = "eatings";
    static String STR03_EN = "An {eater} eats a {eaten}";
    static String STR03_ES = "Un {eater} come una {eaten}";
    static String STR03_RESULT_EN = "An animal eats a plant";
    static String STR03_RESULT_ES = "Un animal come una planta";
    static String STR03_EATER_VAR = "eater";
    static String STR03_EATEN_VAR = "eaten";
    static String STR03_EATER_VAL_EN = "animal";
    static String STR03_EATEN_VAL_EN = "plant";
    static String STR03_EATER_VAL_ES = "animal";
    static String STR03_EATEN_VAL_ES = "planta";

    static String LANG01 = "en";
    static String LANG01_NAME = "English";
    static String LANG02 = "de";
    static String LANG02_NAME = "Deutsch";
    static String LANG03 = "es";
    static String LANG03_NAME = "Español";

    LangStrings baseStrings;

    @BeforeEach
    void setUp() {
        baseStrings = new LangStrings(BASE_YAML, this::baseLangGetter);
    }

    @Test
    void str_stringForLangIsPresent_getsIt() {
        assertThat(baseStrings.str(STR01, 8)).isEqualTo(STR01_EN);
    }

    @Test
    void str_stringForSessionIsNotPresent_returnsNull() {
        assertThat(baseStrings.str(STR01, 888)).isNull();
    }

    @Test
    void str_stringForLangIsNotPresent_returnsNull() {
        assertThat(baseStrings.str(STR_NOT_FOUND, 8)).isNull();
    }

    @Test
    void str_otherStringForLangAreIsPresent_getsIt() {
        assertThat(baseStrings.str(STR02, 8)).isEqualTo(STR02_EN);
    }

    @Test
    void str_otherStringForSessionIsNotPresent_returnsNull() {
        assertThat(baseStrings.str(STR02, 888)).isNull();
    }

    @Test
    void str_wildcardedStringForLangIsPresent_getsIt() {
        assertThat(baseStrings.with(STR03_EATER_VAR, STR03_EATER_VAL_ES)
                              .with(STR03_EATEN_VAR, STR03_EATEN_VAL_ES)
                              .str(STR03, 88)).isEqualTo(STR03_RESULT_ES);
    }

    @Test
    void getLangs_getsThem() {
        Map<String, String> langs = baseStrings.getLangs();

        Map<String, String> expected = new HashMap<>();
        expected.put(LANG01, LANG01_NAME);
        expected.put(LANG02, LANG02_NAME);
        expected.put(LANG03, LANG03_NAME);

        assertThat(langs).isEqualTo(expected);
    }

    String baseLangGetter(Object session) {
        return session.equals(8) ? "en" : (
               session.equals(88) ? "es" :
               null
        );
    }
}
