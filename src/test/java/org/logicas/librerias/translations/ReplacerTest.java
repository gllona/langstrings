package org.logicas.librerias.translations;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ReplacerTest {

    private static final String STR_DONT_MUTATE = "and plants were here again";

    private static final String VAR_NAME = "name";
    private static final String VAL_NAME = "animal";
    private static final String STR_MUTATE_ONE = "lately a plant was eaten by a {name}";
    private static final String STR_MUTATE_ONE_RESULT = "lately a plant was eaten by a animal";

    private static final String STR_MUTATE_COMPLEX_VAR = "{full_name} arose in time with cleverness";
    private static final String STR_MUTATE_COMPLEX_VAR_RESULT = "Drosophila Melanogaster arose in time with cleverness";
    private static final String VAR_FULL_NAME = "full_name";
    private static final String VAL_FULL_NAME = "{genus} {group}";
    private static final String VAR_GENUS = "genus";
    private static final String VAR_GROUP = "group";
    private static final String VAL_GENUS = "Drosophila";
    private static final String VAL_GROUP = "Melanogaster";

    private static final String STR_MUTATE_STARTING = STR_MUTATE_COMPLEX_VAR;
    private static final String STR_MUTATE_STARTING_RESULT = STR_MUTATE_COMPLEX_VAR_RESULT;
    private static final String STR_MUTATE_ENDING = STR_MUTATE_ONE;
    private static final String STR_MUTATE_ENDING_RESULT = STR_MUTATE_ONE_RESULT;
    private static final String STR_MUTATE_INSIDE = "A {full_name} is a winged {name}, right";
    private static final String STR_MUTATE_INSIDE_RESULT = "A Drosophila Melanogaster is a winged animal, right";

    private static final String STR_DONT_MUTATE_BRACES = "{{SummerWind}} was a plant in time";
    private static final String STR_DONT_MUTATE_BRACES_RESULT = "{SummerWind} was a plant in time";
    private static final String VAR_SUMMERWIND = "SummerWind";
    private static final String VAL_SUMMERWIND = "a kind of Survival";

    Replacer replacer;

    @BeforeEach
    void setUp() {
        replacer = new Replacer();
    }

    @Test
    void sub_noWildcardIsPassedForWithoutWildcardsTemplate_dontMutate() {
        String unmutated = replacer.replace(STR_DONT_MUTATE);

        assertThat(unmutated).isEqualTo(STR_DONT_MUTATE);
    }

    @Test
    void sub_wilcardIsPassedForWithoutWildcardsTemplate_dontMutate() {
        String unmutated = replacer
            .with(VAR_NAME, VAL_NAME)
            .replace(STR_DONT_MUTATE);

        assertThat(unmutated).isEqualTo(STR_DONT_MUTATE);
    }

    @Test
    void sub_noWildcardIsPassedForWithWildcardsTemplate_dontMutate() {
        String unmutated = replacer.replace(STR_MUTATE_ONE);

        assertThat(unmutated).isEqualTo(STR_MUTATE_ONE);
    }

    @Test
    void sub_wilcardIsPassedForWithWildcardsTemplate_mutate() {
        String unmutated = replacer
            .with(VAR_NAME, VAL_NAME)
            .replace(STR_MUTATE_ONE);

        assertThat(unmutated).isEqualTo(STR_MUTATE_ONE_RESULT);
    }

    @Test
    void sub_complexWildcard_mutate() {
        String mutated = replacer
            .with(VAR_FULL_NAME, VAL_FULL_NAME)
            .with(VAR_GENUS, VAL_GENUS)
            .with(VAR_GROUP, VAL_GROUP)
            .replace(STR_MUTATE_COMPLEX_VAR);

        assertThat(mutated).isEqualTo(STR_MUTATE_COMPLEX_VAR_RESULT);
    }

    @Test
    void sub_wildcardStarting_mutate() {
        String mutated = replacer
            .with(VAR_FULL_NAME, VAL_FULL_NAME)
            .with(VAR_GENUS, VAL_GENUS)
            .with(VAR_GROUP, VAL_GROUP)
            .replace(STR_MUTATE_STARTING);

        assertThat(mutated).isEqualTo(STR_MUTATE_STARTING_RESULT);
    }

    @Test
    void sub_wildcardEnding_mutate() {
        String mutated = replacer
            .with(VAR_NAME, VAL_NAME)
            .replace(STR_MUTATE_ENDING);

        assertThat(mutated).isEqualTo(STR_MUTATE_ENDING_RESULT);
    }

    @Test
    void sub_wildcardInside_mutate() {
        String mutated = replacer
            .with(VAR_NAME, VAL_NAME)
            .with(VAR_FULL_NAME, VAL_FULL_NAME)
            .with(VAR_GENUS, VAL_GENUS)
            .with(VAR_GROUP, VAL_GROUP)
            .replace(STR_MUTATE_INSIDE);

        assertThat(mutated).isEqualTo(STR_MUTATE_INSIDE_RESULT);
    }

    @Test
    void sub_doubleBraces_dontMutate() {
        String mutated = replacer
            .with(VAR_SUMMERWIND, VAL_SUMMERWIND)
            .replace(STR_DONT_MUTATE_BRACES);

        assertThat(mutated).isEqualTo(STR_DONT_MUTATE_BRACES_RESULT);
    }
}
